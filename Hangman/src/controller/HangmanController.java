package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;

import static java.lang.Character.isLetter;
import static java.lang.Character.toLowerCase;
import static java.lang.Character.toUpperCase;
import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee, Jenny Li
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Text[]      progress;    // reference to the text area for the word
    private GridPane    guesses;
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private ArrayList   letters;     // array of distinct letters
    private ArrayList<Shape> man;    // array of shapes of hangman
    private Button      gameButton;  // shared reference to the "start game" button
    private Button      hintButton;  // shared reference to the "get hint" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private Path        workFile;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    public void updateHintButton() {
        Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
        hintButton = (Button) workspace.getGameTextsPane().getChildren().get(3);
        int distinctLetters = getDistinctLetters();
        if (distinctLetters > 7) {
            hintButton.setVisible(true);
            hintButton.setDisable(gamedata.isHintUsed());
        }
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */
    public void start() {
        gamedata = (GameData) appTemplate.getDataComponent();
        success = false;
        discovered = 0;

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();

        gamedata.init();
        updateHintButton();
        hintButton.setOnMouseClicked(e -> getHint());
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Pane figurePane = gameWorkspace.getFigurePane();
        initManGraphics(figurePane);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        guessedLetters.setSpacing(5);
        initWordGraphics(guessedLetters);
        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);

        guesses = (GridPane) gameWorkspace.getGameTextsPane().getChildren().get(2);
        guesses.setHgap(5);
        guesses.setVgap(5);
        initGuessGraphics(guesses);
        play();
    }

    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameButton.setDisable(true);
        hintButton.setDisable(true);
        setGameState(GameState.ENDED);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            if (dialog.isShowing())
                dialog.toFront();
            else
                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
        });
        if (!success) {
            for (int i = 0; i < progress.length; i++) {
                if (!alreadyGuessed(gamedata.getTargetWord().charAt(i))) {
                    progress[i].setVisible(true);
                    progress[i].setFill(Color.RED);
                }
            }
        }
    }

    private void initWordGraphics(HBox guessedLetters) {
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);
            StackPane pane = new StackPane();
            Rectangle rect = new Rectangle(30, 30, Color.BEIGE);
            rect.setStroke(Color.BLACK);
            pane.getChildren().add(rect);
            pane.getChildren().add(progress[i]);
            guessedLetters.getChildren().add(pane);
        }
    }

    private void initGuessGraphics(GridPane guesses) {
        char[] alphabet = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
        int i = 0;
        for (int j = 0; j < 4; j++) {
            for (int k = 0; k < 7; k++) {
                StackPane pane = new StackPane();
                Rectangle rect = new Rectangle(30, 30, Color.GREEN);
                rect.setStroke(Color.BLACK);
                pane.getChildren().add(rect);
                pane.getChildren().add(new Label(""+alphabet[i]));
                pane.setVisible(false);
                guesses.add(pane, k, j);
                i++;
                if (i == 26)
                    break;
            }
        }
    }

    private void initManGraphics(Pane figurePane) {
        man = new ArrayList<>(10);
        Circle head = new Circle(50);
        head.setCenterX(175);
        head.setCenterY(100);
        head.setStroke(Color.BLACK);
        head.setFill(null);
        head.setVisible(false);
        double bottHead = head.getCenterY()+head.getRadius();
        double topHead = head.getCenterY()-head.getRadius();
        Line torso = new Line(head.getCenterX(), bottHead, head.getCenterX(), bottHead+70);
        torso.setStroke(Color.BLACK);
        torso.setVisible(false);
        Line leftArm = new Line(head.getCenterX(), bottHead+10, head.getCenterX()-25, bottHead+45);
        leftArm.setStroke(Color.BLACK);
        leftArm.setVisible(false);
        Line rightArm = new Line(head.getCenterX(), bottHead+10, head.getCenterX()+25, bottHead+45);
        rightArm.setStroke(Color.BLACK);
        rightArm.setVisible(false);
        Line leftLeg = new Line(head.getCenterX(), bottHead+70, head.getCenterX()-25, bottHead+95);
        leftLeg.setStroke(Color.BLACK);
        leftLeg.setVisible(false);
        Line rightLeg = new Line(head.getCenterX(), bottHead+70, head.getCenterX()+25, bottHead+95);
        rightLeg.setStroke(Color.BLACK);
        rightLeg.setVisible(false);
        figurePane.getChildren().addAll(head, torso, leftArm, rightArm, leftLeg, rightLeg);
        Line hair = new Line(head.getCenterX(), 0, head.getCenterX(), topHead);
        hair.setStroke(Color.BLACK);
        hair.setVisible(false);
        Line topHorizontal = new Line(75, 0, head.getCenterX(), 0);
        topHorizontal.setStroke(Color.BLACK);
        topHorizontal.setVisible(false);
        Line vertical = new Line(75, 0, 75, bottHead+125);
        vertical.setStroke(Color.BLACK);
        vertical.setVisible(false);
        Line bottHorizontal = new Line(50, bottHead+125, 250, bottHead+125);
        bottHorizontal.setStroke(Color.BLACK);
        bottHorizontal.setVisible(false);
        figurePane.getChildren().addAll(hair, topHorizontal, vertical, bottHorizontal);
        man.add(0, rightLeg);
        man.add(1, leftLeg);
        man.add(2, rightArm);
        man.add(3, leftArm);
        man.add(4, torso);
        man.add(5, head);
        man.add(6, hair);
        man.add(7, topHorizontal);
        man.add(8, vertical);
        man.add(9, bottHorizontal);
    }

    public void play() {
        disableGameButton();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = event.getCharacter().charAt(0);
                    if (!isLetter(guess)) {
                        Platform.runLater(() -> {
                            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
                            String                    message    = "Invalid key entered. Please enter a letter.";
                            if (dialog.isShowing())
                                dialog.toFront();
                            else
                                dialog.show("Bad Key", message);
                        });
                    }
                    if (isLetter(guess)) {
                        guess = toLowerCase(guess);
                        if (!alreadyGuessed(guess)) {
                            boolean goodguess = false;
                            for (int i = 0; i < progress.length; i++) {
                                if (gamedata.getTargetWord().charAt(i) == guess) {
                                    progress[i].setVisible(true);
                                    gamedata.addGoodGuess(guess);
                                    goodguess = true;
                                    discovered++;
                                }
                            }
                            if (!goodguess) {
                                gamedata.addBadGuess(guess);
                                man.get(gamedata.getRemainingGuesses()).setVisible(true);
                            }
                            guesses.getChildren().get(toUpperCase(guess) - 65).setVisible(true);

                            success = (discovered == progress.length);
                            remains.setText(Integer.toString(gamedata.getRemainingGuesses()));

                        }
                        setGameState(GameState.INITIALIZED_MODIFIED);
                    }
                });
                if (gamedata.getRemainingGuesses() == 1) {
                    gamedata.setHintUsed(true);
                    hintButton.setDisable(gamedata.isHintUsed());
                }
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();
            }

            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    private int getDistinctLetters() {
        String targetWord = gamedata.getTargetWord();
        int distinctLetters = 0;
        letters = new ArrayList();
        for (int i = 0; i < targetWord.length(); i++) {
            if (!letters.contains(targetWord.charAt(i))) {
                letters.add(targetWord.charAt(i));
                distinctLetters++;
            }
        }
        return distinctLetters;
    }

    public void getHint() {
        int rand;
        char letter;
        boolean goodLetter = false;
        do {
            rand = new Random().nextInt(getDistinctLetters());
            letter = ((char) letters.get(rand));
            if (!alreadyGuessed(letter))
                goodLetter = true;
        } while (!goodLetter);
        for (int i = 0; i < gamedata.getTargetWord().length(); i++) {
            if (gamedata.getTargetWord().charAt(i) == letter) {
                progress[i].setVisible(true);
                gamedata.addGoodGuess(letter);
                discovered++;
            }
        }
        guesses.getChildren().get(toUpperCase(letter) - 65).setVisible(true);
        gamedata.setRemainingGuesses(gamedata.getRemainingGuesses()-1);
        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
        man.get(gamedata.getRemainingGuesses()).setVisible(true);
        gamedata.setHintUsed(true);
        hintButton.setDisable(gamedata.isHintUsed());
        setGameState(GameState.INITIALIZED_MODIFIED);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
    }

    private void restoreGUI() {
        disableGameButton();
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();

        updateHintButton();

        Pane figurePane = gameWorkspace.getFigurePane();
        HBox guessedLetters = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        guessedLetters.setSpacing(5);

        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);

        guesses = (GridPane) gameWorkspace.getGameTextsPane().getChildren().get(2);
        guesses.setHgap(5);
        guesses.setVgap(5);

        restoreWordGraphics(guessedLetters);
        restoreGuessGraphics(guesses);
        restoreManGraphics(figurePane);

        success = false;
        play();
    }

    private void restoreWordGraphics(HBox guessedLetters) {
        discovered = 0;
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));
            if (progress[i].isVisible())
                discovered++;
            StackPane pane = new StackPane();
            Rectangle rect = new Rectangle(30, 30, Color.BEIGE);
            rect.setStroke(Color.BLACK);
            pane.getChildren().add(rect);
            pane.getChildren().add(progress[i]);
            guessedLetters.getChildren().add(pane);
        }
    }

    private void restoreGuessGraphics(GridPane guesses) {
        char[] alphabet = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
        int i = 0;
        for (int j = 0; j < 4; j++) {
            for (int k = 0; k < 7; k++) {
                StackPane pane = new StackPane();
                Rectangle rect = new Rectangle(30, 30, Color.GREEN);
                rect.setStroke(Color.BLACK);
                pane.getChildren().add(rect);
                pane.getChildren().add(new Label(""+alphabet[i]));
                if (alreadyGuessed(toLowerCase(alphabet[i])))
                    pane.setVisible(true);
                else
                    pane.setVisible(false);
                guesses.add(pane, k, j);
                i++;
                if (i == 26)
                    break;
            }
        }
    }

    private void restoreManGraphics(Pane figurePane) {
        man = new ArrayList<>(10);
        Circle head = new Circle(50);
        head.setCenterX(175);
        head.setCenterY(100);
        head.setStroke(Color.BLACK);
        head.setFill(null);
        head.setVisible(false);
        double bottHead = head.getCenterY()+head.getRadius();
        double topHead = head.getCenterY()-head.getRadius();
        Line torso = new Line(head.getCenterX(), bottHead, head.getCenterX(), bottHead+70);
        torso.setStroke(Color.BLACK);
        torso.setVisible(false);
        Line leftArm = new Line(head.getCenterX(), bottHead+10, head.getCenterX()-25, bottHead+45);
        leftArm.setStroke(Color.BLACK);
        leftArm.setVisible(false);
        Line rightArm = new Line(head.getCenterX(), bottHead+10, head.getCenterX()+25, bottHead+45);
        rightArm.setStroke(Color.BLACK);
        rightArm.setVisible(false);
        Line leftLeg = new Line(head.getCenterX(), bottHead+70, head.getCenterX()-25, bottHead+95);
        leftLeg.setStroke(Color.BLACK);
        leftLeg.setVisible(false);
        Line rightLeg = new Line(head.getCenterX(), bottHead+70, head.getCenterX()+25, bottHead+95);
        rightLeg.setStroke(Color.BLACK);
        rightLeg.setVisible(false);
        figurePane.getChildren().addAll(head, torso, leftArm, rightArm, leftLeg, rightLeg);
        Line hair = new Line(head.getCenterX(), 0, head.getCenterX(), topHead);
        hair.setStroke(Color.BLACK);
        hair.setVisible(false);
        Line topHorizontal = new Line(75, 0, head.getCenterX(), 0);
        topHorizontal.setStroke(Color.BLACK);
        topHorizontal.setVisible(false);
        Line vertical = new Line(75, 0, 75, bottHead+125);
        vertical.setStroke(Color.BLACK);
        vertical.setVisible(false);
        Line bottHorizontal = new Line(50, bottHead+125, 250, bottHead+125);
        bottHorizontal.setStroke(Color.BLACK);
        bottHorizontal.setVisible(false);
        figurePane.getChildren().addAll(hair, topHorizontal, vertical, bottHorizontal);
        man.add(0, rightLeg);
        man.add(1, leftLeg);
        man.add(2, rightArm);
        man.add(3, leftArm);
        man.add(4, torso);
        man.add(5, head);
        man.add(6, hair);
        man.add(7, topHorizontal);
        man.add(8, vertical);
        man.add(9, bottHorizontal);
        for (int i = 9; i >=0; i--) {
            if (gamedata.getRemainingGuesses() < i+1)
                man.get(i).setVisible(true);
        }
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            targetPath.toFile().getParentFile().mkdirs();
            targetPath.toFile().mkdir();
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            load = promptToSave();
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser     filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            targetPath.toFile().getParentFile().mkdirs();
            targetPath.toFile().mkdir();
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists()) {
                load(selectedFile.toPath());
                restoreGUI(); // restores the GUI to reflect the state in which the loaded game was last saved
            }
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                               propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }
}
